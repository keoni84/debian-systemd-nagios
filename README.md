# README #

This is a test for running nagios on a systemd enabled debian container

### What is this repository for? ###

container running nagios on debian with systemd enabled

* sshd is also enabled with default config - you will need to shell into the running container and create users

* nagios or sshd can be restarted via systemctl

* symbolic link created for /nagios -> /usr/local/nagios

### To run use ###

docker run --privileged -p 80:80 -d -ti -v /sys/fs/cgroup:/sys/fs/cgroup:ro keoni84/debian-systemd-nagios

* apache will be listening on port 80 (change as needed)

* nagios can be accessed via http://localhost/nagios

* nagios login: nagiosadmin/1d0ntkn0w

### To launch container with sshd port listening on port 2222 ###

docker run --privileged -p 80:80 -p 2222:22 -d -ti -v /sys/fs/cgroup:/sys/fs/cgroup:ro keoni84/debian-systemd-nagios

* Change ports as needed