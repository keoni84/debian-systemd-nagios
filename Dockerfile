FROM keoni84/debian-systemd

ENV DEBIAN_FRONTEND noninteractive
ENV container docker

MAINTAINER keoni84 <keoni84@gmail.com>

# -----------------------------------------------------------------------------
# Install required packages & nagios user creation
# -----------------------------------------------------------------------------
RUN apt-get install -y autoconf gcc libc6 make wget vim-tiny unzip apache2 apache2-utils \
php7.1 snmp sudo dnsutils libmonitoring-plugin-perl libsocket-perl libio-socket-ssl-perl \
libio-socket-ip-perl libio-socket-timeout-perl \libxml-simple-perl libnet-snmp-perl \
autoconf gcc libc6 exim4 libmcrypt-dev make libssl-dev bc gawk dc gettext openssh-server && \
useradd -u 1001 nagios && usermod -a -G nagios www-data && apt-get clean

# -----------------------------------------------------------------------------
# Install nagios & apache
# -----------------------------------------------------------------------------
RUN wget https://bitbucket.org/keoni84/debian-systemd-nagios/downloads/nagios-4.3.2.tar.gz -O /tmp/nagios-4.3.2.tar.gz && \
mkdir /tmp/nagios && tar zxf /tmp/nagios-4.3.2.tar.gz -C /tmp/nagios && cd /tmp/nagios/nagios-4.3.2 && \
/tmp/nagios/nagios-4.3.2/configure --with-httpd-conf=/etc/apache2/sites-enabled && \
make all && make install && make install-init && make install-config && make install-webconf && \
a2enmod rewrite && a2enmod cgi && htpasswd -b -c /usr/local/nagios/etc/htpasswd.users nagiosadmin 1d0ntkn0w && \
cd / && systemctl enable nagios.service && systemctl enable apache2 && systemctl enable exim4 && rm -rf /tmp/nagios*

# -----------------------------------------------------------------------------
# Install nagios plug-ins & other configs
# -----------------------------------------------------------------------------
RUN wget https://bitbucket.org/keoni84/debian-systemd-nagios/downloads/nagios-plugins-2.2.1.tar.gz -O /tmp/nagios-plugins-2.2.1.tar.gz && \
mkdir /tmp/nagios && tar zxf /tmp/nagios-plugins-2.2.1.tar.gz -C /tmp/nagios && cd /tmp/nagios/nagios-plugins-2.2.1 && \
/tmp/nagios/nagios-plugins-2.2.1/configure && make && make install && \
mkdir /usr/local/nagios/var/rw && chown nagios:nagios /usr/local/nagios/var/rw && \
cd / && ln -s /usr/local/nagios nagios && echo "alias ll='ls -l'" >> /etc/bash.bashrc && rm -rf /tmp/nagios*

# -----------------------------------------------------------------------------
# Expose port 22
# -----------------------------------------------------------------------------
EXPOSE 22
EXPOSE 80

# -----------------------------------------------------------------------------
# Expose volumes
# -----------------------------------------------------------------------------
VOLUME [ "/sys/fs/cgroup", "/run", "/run/lock", "/tmp" ]

# -----------------------------------------------------------------------------
# Command to init
# -----------------------------------------------------------------------------
CMD ["/lib/systemd/systemd"]